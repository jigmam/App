from pydantic import BaseModel


class IdentifierCreate(BaseModel):
    name: str


class Identifier(IdentifierCreate):
    id: int

    class Config:
        orm_mode = True

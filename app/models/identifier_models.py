from sqlalchemy import Column, Integer, Sequence, String
from sqlalchemy.orm import relationship

from config.database import Base


class Identifier(Base):
    __tablename__ = "identifier"
    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    name = Column(String)

    histories = relationship("History", back_populates="identifier")

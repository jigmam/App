from datetime import date
from pydantic import BaseModel


class History(BaseModel):
    date: date
    id_matter: int
    id_identifier: int
    value: float

    class Config:
        orm_mode = True


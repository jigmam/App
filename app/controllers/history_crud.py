from sqlalchemy.orm import Session
from datetime import date
from models import history_models
from models.history import History
from utils.mappers import mapper_operator


def create_history(db: Session, history:History):
    history_db = history_models.History(date=history.date, id_matter=history.id_matter, id_identifier=history.id_identifier, value=history.value)
    print(history_db)
    db.add(history_db)
    db.commit()
    db.refresh(history_db)
    return history_db

def get_history(db: Session, skip: int, limit: int):
    return db.query(history_models.History).offset(skip).limit(limit).all()


def get_history_by_id(db: Session,
                      skip: int = 0,
                      limit: int = 100,
                      date: date = None,
                      date_com: str = '==',
                      matter_id: int = None,
                      matter_com: str = '==',
                      identifier_id: int = None,
                      identifier_com: str = '=='):
    db_query = db.query(history_models.History)
    if (date is not None):
        db_query = db_query.filter(mapper_operator(history_models.History.date, date, date_com))
    if (identifier_id is not None):
        db_query = db_query.filter(mapper_operator(history_models.History.id_identifier, identifier_id, identifier_com))
    if (matter_id is not None):
        db_query = db_query.filter(mapper_operator(history_models.History.id_matter, matter_id, matter_com))
    print(db_query)
    return db_query.offset(skip).limit(limit).all()

def update_history(db: Session, date: date, id_history: int, value: int):
    history_db = db.query(history_models.History).filter(history_models.History.date == date, history_models.History.id_history == id_history).first()
    history_db.value = value
    db.commit()
    return history_db

def delete_history(db: Session, id_history: int):
    history_db = db.query(history_models.History).filter(history_models.History.id_history == id_history).first()
    if not history_db:
        return None
    db.delete(history_db)
    db.commit()
    return history_db
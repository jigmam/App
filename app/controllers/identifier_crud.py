from typing import List
from sqlalchemy.orm import Session
from models import identifier_models
from models.identifier import Identifier


def create_identifier(db: Session, identifier: Identifier):
    identifier_db = identifier_models.Identifier(name=identifier.name)
    db.add(identifier_db)
    db.commit()
    db.refresh(identifier_db)
    return identifier_db


def get_identifier(db: Session, skip: int = 0, limit: int = 100) -> List[Identifier]:
    return db.query(identifier_models.Identifier).offset(skip).limit(limit).all()


def get_identifier_by_id(db: Session, id_identifier: int):
    return db.query(identifier_models.Identifier).filter(
        identifier_models.Identifier.id == id_identifier).first()


def update_identifier(db: Session, id_identifier: int, name: str):
    identifier_db = db.query(identifier_models.Identifier).filter(
        identifier_models.Identifier.id == id_identifier).first()
    identifier_db.name = name
    db.commit()
    return identifier_db


def delete_identifier(db: Session, id_identifier: int):
    identifier_db = db.query(identifier_models.Identifier).filter(
        identifier_models.Identifier.id == id_identifier).first()
    db.delete(identifier_db)
    db.commit()

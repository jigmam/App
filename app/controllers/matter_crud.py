from collections import defaultdict
from sqlalchemy.orm import Session
from models.matter_models import Matter
from querys.matter_querys import query_get_matter_and_submatter_by_user, query_get_matter_parameter
from models.matter import MatterCreate
from sqlalchemy import text

def create_matter(db: Session, matter: MatterCreate):
    matter_db = Matter(name=matter.name, user_id=matter.user_id)
    db.add(matter_db)
    db.commit()
    db.refresh(matter_db)
    return matter_db

def get_matter(db: Session, skip: int, limit: int):
    return db.query(Matter).limit(limit).offset(skip).all()

def get_matter_by_id(db: Session, matter_id: int):
    return db.query(Matter).filter(Matter.id == matter_id).first()

def get_matter_by_user(db: Session, user_id: int):
    query = text(query_get_matter_parameter(user_id, 1))
    matter_data = db.execute(query).mappings().all()
    return matter_data

def get_matter_by_user_id(db: Session, user_id: int):
    query = text(query_get_matter_and_submatter_by_user(user_id))
    matter_by_user = db.execute(query).mappings().all()
    # Agrupar por id de materia prima
    groups = defaultdict(list)
    for d in matter_by_user:
        id_mp = d['id']
        mp = d['name']
        qty = d['required']
        secondary_id = d['secondary_id']
        secondary_name = d['secondary_name']
        user_id = d['user_id']
        groups[id_mp].append((mp, qty, secondary_id, secondary_name, user_id))

    # Combinar elementos con el mismo id de materia prima
    result = []
    query = text(query_get_matter_parameter(user_id, 1))
    matter_data = db.execute(query).mappings().all()
    matter_data = {d['id_matter']: d for d in matter_data}
    for id_mp, group in groups.items():
        mp = group[0][0]
        sum_data = 0
        parts = []
        for _, qty, secondary_id, secondary_name, _ in group:
            submatter = matter_data.get(secondary_id)
            cost = submatter['value'] if submatter is not None else 0
            sum_data += cost * float(qty)
            parts.append(
                {
                    'secondary_id': secondary_id,
                    'secondary_name': secondary_name,
                    'required': qty,
                    'cost': cost
                }
            )
        user_id = group[0][4]
        result.append({'id': id_mp, 'name': mp,
                       'secondary': parts, 'total_price': sum_data, 'user_id': user_id})

    return result

def update_matter(db: Session, matter_db: Matter, name: str):
    matter_db.name = name
    db.commit()
    return matter_db

def delete_matter(db: Session, matter_db: Matter):
    db.delete(matter_db)
    db.commit()

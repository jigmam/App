import operator

ops = {
        "==": operator.eq,
        ">=": operator.ge,
        "<=": operator.le,
        "!=": operator.ne,
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
    }


def mapper_operator(a, b, map: str):
    print(a, b, map)
    op_func = ops[map]
    return op_func(a, b)
